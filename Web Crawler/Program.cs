﻿using System;
using System.Threading;
using System.Net.Http;
using HtmlAgilityPack;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Threading.Tasks;

//TODO: Write function to create date string
namespace Web_Crawler
{
    class Program
    {

        //https://nos.nl/artikel/2372279-coronacrisis-houdt-attractieparken-al-jaar-in-de-greep-wij-kunnen-veilig-open.html
        static StreamWriter streamWriter3;
        static List<Day> days;
        readonly static List<string> primaryPandemic = new List<string>() { "corona", "covid", "sars-cov-2" };
        readonly static List<string> secondaryPandemic = new List<string>() { "vaccin", "avondklok", "lockdown", "maatregelen", "ziekenhuisopnames", "bron- en contactonderzoek", "persconferentie", "besmetting" };
        readonly static List<string> primarySource = new List<string>() { "rivm", "ministerie van volksgezondheid", "coronadashboard", "minister de jonge", "rutte", "minister bruins", "diederik gommers", "outbreak management team", "omt", "van dissel", "jaap van delden", "ernst kuipers", "world health organisation", "who", "europees geneesmiddelenbureau", "european medicines agency", "ema", "emea", "landelijk coördinatiecentrum patiënten versprijding", "lcps", "college ter beoordeling van geneesmiddelen", "cbg", "ggd", "grapperhaus", "persconferentie" };
        readonly static List<string> secondarySource = new List<string>() { "onderzoek", "toezichthouder", "europese laden", "hoogleraar", "epidemioloog", "viroloog", "besmettingscijfers", "immunoloog", "arts", "woordvoerder" };
        readonly static List<string> tertiarySource = new List<string>() { "volgens", "stelt", "aldus", "zegt", "zeggen", "blijkt", "meldt" };
        static void Main(string[] args)
        {
            string input;
            bool nu = false;
            do
            {
                Console.WriteLine("NOS or NU?");
                input = Console.ReadLine().ToLower();
                if (input == "nu") nu = true;
                else if (input == "nos") nu = false;
                else Console.WriteLine("Expected NOS or NU, try again.");
            }
            while (input != "nos" && input != "nu");

            string nuOrNos;
            if (nu) nuOrNos = "nu";
            else nuOrNos = "nos";

            XmlSerializer serializer = new XmlSerializer(typeof(List<Day>));
            Console.WriteLine("Do you want to attempt to deserialize the loaded articles? Type 'T' to deserialize, type 'F' to run the webcrawler from scratch"); input = Console.ReadLine().ToLower();
            while (input != "t" && input != "f")
            {
                Console.WriteLine("Invalid Argument");
                Console.WriteLine("Do you want to attempt to deserialize the loaded articles? Type 'T' to deserialize, type 'F' to run the webcrawler from scratch");
                input = Console.ReadLine().ToLower();
            }
            bool serialize = false;
            if (input == "t") serialize = true;
            if (serialize && File.Exists(Directory.GetCurrentDirectory().Remove(Directory.GetCurrentDirectory().Length - 24) + "\\Articles\\" + nuOrNos + "Days.xml"))
            {
                Stream stream1 = File.OpenRead(Directory.GetCurrentDirectory().Remove(Directory.GetCurrentDirectory().Length - 24) + "\\Articles\\" + nuOrNos + "Days.xml");
                days = (List<Day>)serializer.Deserialize(stream1);
                stream1.Close();
                Console.WriteLine("File loaded");
            }
            else
            {
                if (serialize) Console.WriteLine("Deserialization failed, " + nuOrNos + "Days.txt does not exist, running webcrawler");
                days = new List<Day>();
                if (nu)
                {
                    days = StartCrawlerNUpuntNL().ToDays(primaryPandemic, secondaryPandemic);
                }
                else
                {
                    List<DateTime> dates = GetDates();
                    foreach (DateTime date in dates)
                    {
                        days.Add(StartCrawlerNOS(date));
                    }
                }
            }

            streamWriter3 = new StreamWriter(Directory.GetCurrentDirectory().Remove(Directory.GetCurrentDirectory().Length - 24) + "\\Articles\\" + nuOrNos + "Occurences.csv");
            streamWriter3.Write("Date,Primary Corona Words,Secondary Corona Words,Primary Sources Primary Articles,Primary Sources Secondary Articles,Secondary Sources Primary Articles, Secondary Sources Secondary Articles,Tertiary Sources Primary Articles, Tertiary Sources Secondary Articles");
            foreach (string s in primaryPandemic)
                streamWriter3.Write("," + s);
            foreach (string s in secondaryPandemic)
                streamWriter3.Write("," + s);
            foreach (string s in primarySource)
            {
                streamWriter3.Write("," + s + " in Primary");
                streamWriter3.Write("," + s + " in Secondary");
            }
            foreach (string s in secondarySource)
            {
                streamWriter3.Write("," + s + " in Primary");
                streamWriter3.Write("," + s + " in Secondary");
            }
            foreach (string s in tertiarySource)
            {
                streamWriter3.Write("," + s + " in Primary");
                streamWriter3.Write("," + s + " in Secondary");
            }
            streamWriter3.WriteLine();
            foreach (Day d in days)
                d.Print(streamWriter3, primaryPandemic, secondaryPandemic, primarySource, secondarySource, tertiarySource);
            streamWriter3.Close();
            int i = 0;
            foreach (Day d in days)
            {
                i += d.Articles.Count;
            }
            Console.WriteLine(i);
            File.Delete(Directory.GetCurrentDirectory().Remove(Directory.GetCurrentDirectory().Length - 24) + "\\Articles\\" + nuOrNos + "Days.xml");
            Stream stream = File.OpenWrite(Directory.GetCurrentDirectory().Remove(Directory.GetCurrentDirectory().Length - 24) + "\\Articles\\" + nuOrNos + "Days.xml");
            serializer.Serialize(stream, days);
            stream.Close();
        }

        private static ArchiveNUpuntNL StartCrawlerNUpuntNL()
        {
            // https://www.nu.nl/jaarwisseling/6021049/hoofdprijs-staatsloterij-van-30-miljoen-valt-in-enschede-en-krommenie.html?redirect=1
            // Eerste article van Nu.nl in 2020
            // https://www.nu.nl/sitemap_index.xml sitemap nu.nl
            //Console.WriteLine(date.Date.ToString("yyyy-MM-dd"));
            ArchiveNUpuntNL archief = new ArchiveNUpuntNL();
            HttpClient httpClient = new HttpClient();
            int startindex = 6021049;
            int endindex = 6127000;
            bool[] isRecieved = new bool[endindex - startindex];
            Task<string>[] allTasks = new Task<string>[endindex - startindex];
            string urlBase = "https://www.nu.nl/jaarwisseling/"; // waar jaarwisseling staat boeit niet wat er staat zolang er maar iets staat.
            int loopCount = 0;
            while (isRecieved.Contains(false))
            {
                loopCount++;
                Console.WriteLine("Loop " + loopCount);
                int taskCount = 0;
                Console.Write("Receiving articles");
                for (int urlIndex = startindex; urlIndex < endindex; urlIndex++)
                {
                    if (!isRecieved[urlIndex - startindex])
                    {
                        allTasks[urlIndex - startindex] = httpClient.GetStringAsync(urlBase + urlIndex);
                        taskCount++;
                    }
                    int arrayLength = 200;
                    if (taskCount != 0 && taskCount % Math.Floor((float)arrayLength / 3) == 0)
                        Console.Write('.');
                    if (taskCount > arrayLength)
                        break;
                }
                Console.WriteLine("Evaluating html...");
                for (int urlIndex = startindex; urlIndex < endindex; urlIndex++)
                {
                    if (isRecieved[urlIndex - startindex])
                        continue;
                    if (allTasks[urlIndex - startindex] == null)
                        break;
                    string html = "null";
                    try
                    {
                        html = allTasks[urlIndex - startindex].Result;
                    }
                    catch (AggregateException e)
                    {
                        if (e.Message.Contains("404"))
                        {
                            Console.WriteLine(urlIndex + ": exception, our fault, skip (404)");
                            isRecieved[urlIndex - startindex] = true;
                            continue;
                        }
                        if (!e.Message.Contains("500") && !e.Message.Contains("501") && !e.Message.Contains("502") && !e.Message.Contains("503") && !e.Message.Contains("408"))
                        {
                            Console.WriteLine(e.Message);
                            isRecieved[urlIndex - startindex] = true;
                            continue;
                        }
                        Console.WriteLine("exception, not our fault, trying again");
                        continue;
                    }
                    isRecieved[urlIndex - startindex] = true;
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(html);
                    if (htmlDocument.DocumentNode.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("caption")).ToList().Count() > 0)
                        continue;
                    string title = htmlDocument.DocumentNode.Descendants("h1").Where(node => node.GetAttributeValue("class", "").Equals("title fluid")).ToList().First().InnerText;
                    string datum = htmlDocument.DocumentNode.Descendants("span").Where(node => node.GetAttributeValue("class", "").Equals("pubdate small")).ToList().First().InnerText;
                    string body = "";
                    List<HtmlNode> tmp = htmlDocument.DocumentNode.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("block article-body")).ToList().First().Descendants("p").ToList();
                    foreach (HtmlNode s in tmp)
                    {
                        body += s.InnerText;
                    }
                    Console.WriteLine(urlIndex + " " + datum + ": " + title);
                    archief.articles.Add(new ArticleNUpuntNLPreProcessing()
                    {
                        Title = title,
                        Date = datum,
                        Body = body
                    });
                }
            }
            return archief;
        }

        private static Day StartCrawlerNOS(DateTime date, bool writeToFile = false, StreamWriter streamWriter3 = null)
        {
            Console.WriteLine(date.Date.ToString("yyyy-MM-dd"));
            Day day = new Day(new List<Article>(), date, new List<Article>(), new List<Article>());
            HttpClient httpClient = new HttpClient();
            string html = httpClient.GetStringAsync("https://nos.nl/nieuws/archief/" + date.Date.ToString("yyyy-MM-dd") + "/").Result;
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            List<HtmlNode> links = htmlDocument.DocumentNode.Descendants("a").Where(node => node.GetAttributeValue("class", "").Equals("link-block")).ToList();
            bool[] recevied = new bool[links.Count];
            Task<string>[] allTasks = new Task<string>[links.Count];

            while (recevied.Contains(false))
            {
                for (int i = 0; i < recevied.Length; i++)
                {
                    if (recevied[i])
                        continue;

                    string article_url = links[i].GetAttributeValue("href", "no link");
                    if (article_url != "no link" && !article_url.Contains("liveblog"))
                    {
                        allTasks[i] = (httpClient.GetStringAsync("https://nos.nl" + article_url));
                    }
                    else
                    {
                        recevied[i] = true;
                    }
                }
                for (int i = 0; i < recevied.Length; i++)
                {
                    if (recevied[i])
                        continue;
                    //string article_url = n.GetAttributeValue("href", "no link");
                    //if (article_url != "no link")
                        //bool success = false;
                        //while (!success)
                        //{
                            try
                            {
                                day.AddIfNotNull(FollowLinkNos(allTasks[i]), primaryPandemic, secondaryPandemic);
                                //success = true;
                                recevied[i] = true;
                            }
                            catch (AggregateException e)
                            {
                                if (!e.Message.Contains("500") && !e.Message.Contains("501") && !e.Message.Contains("502") && !e.Message.Contains("503") && !e.Message.Contains("408"))
                                {
                                    Console.WriteLine("EXCEPTION");
                                    Console.WriteLine(e.Message);
                                    break;
                                }
                                Console.WriteLine("exception, not our fault, trying again");
                            }

                        //}
                    //}
                }
            }
            return day;
        }

        private static Article FollowLinkNos(/*string url,*/ /*DateTime date,*/ Task<string> httpclientTask)
        {
            string headWrapper = "title_iP7Q1aiP";
            string content = "contentBlock_6FeUmGxw";
            //url = "https://nos.nl" + url;
            //if (url.Contains("liveblog"))
            //    return FollowLiveBlogNOS(url);

            //string html = httpClient.GetStringAsync(url).Result;
            string html = httpclientTask.Result;
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            string title = htmlDocument.DocumentNode.Descendants("h1").Where(node => node.GetAttributeValue("class", "").Equals(headWrapper)).ToList().First().InnerText;
            string body = "";
            List<HtmlNode> bodyNodes = htmlDocument.DocumentNode.Descendants("p").Where(node => node.GetAttributeValue("class", "").Equals("text_3v_J6Y0G")).ToList();
            foreach (HtmlNode n in bodyNodes)
                body += n.InnerText + "\n";
            Console.Write(title);
            return new Article(title, body);
        }

        private static Article FollowLiveBlogNOS(string url)
        {
            return null;
        }

        private static List<DateTime> GetDates()
        {
            return GetDates(new DateTime(2020, 1, 1));
        }

        private static List<DateTime> GetDates(DateTime d)
        {
            List<DateTime> dates = new List<DateTime>();
            while (d.Date != DateTime.Now.Date)
            {
                dates.Add(d);
                d = d.AddDays(1);
            }
            return dates;
        }
    }
}
