﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Web_Crawler
{
    public class Article
    {
        public string Title;
        public string Body;

        public Article() { }

        public Article(string title, string body)
        {
            Title = title.ToLower();
            Body = body.ToLower();
        }

        public bool Contains(string word)
        {
            return Title.Contains(word) || Body.Contains(word);
        }

        public int CountWords(string searchTerm)
        {
            int count = 0;
            int a = 0;
            while ((a = Body.IndexOf(searchTerm, a)) != -1)
            {
                a += searchTerm.Length;
                count++;
            }
            a = 0;
            while ((a = Title.IndexOf(searchTerm, a)) != -1)
            {
                a += searchTerm.Length;
                count++;
            }
            return count;
        }
    }

    public class Day : IComparable
    {
        public List<Article> Articles;
        public DateTime Date;
        public List<Article> FirstOrderCoronaArticles;
        public List<Article> SecondOrderCoronaArticles;
        public Day() { }

        public Day(List<Article> Articles, DateTime Date, List<Article> FirstOrderCoronaArticles, List<Article> SecondOrderCoronaArticles)
        {
            this.Articles = Articles;
            this.Date = Date;
            this.FirstOrderCoronaArticles = FirstOrderCoronaArticles;
            this.SecondOrderCoronaArticles = SecondOrderCoronaArticles;
        }

        public void AddArticle(Article a, List<string> primaryPandemic, List<string> secondaryPandemic)
        {
            Articles.Add(a);
            bool found = false;
            foreach (string s in primaryPandemic)
            {
                if (a.Contains(s))
                {
                    FirstOrderCoronaArticles.Add(a);
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                foreach (string s in secondaryPandemic)
                {
                    if (a.Contains(s))
                    {
                        SecondOrderCoronaArticles.Add(a);
                        break;
                    }
                }
            }
        }

        public bool AddIfNotNull(Article a, List<string> primaryPandemic, List<string> secondaryPandemic)
        {
            if (a == null) return false;
            AddArticle(a, primaryPandemic, secondaryPandemic);
            return true;
        }

        public int CompareTo(object obj)
        {

            if (obj == null || obj.GetType() != typeof(Day))
                return 1;

            Day day = obj as Day;

            if (Date.Ticks >= day.Date.Ticks)
                return 1;
            return -1;

        }

        public int CountWords(string word)
        {
            int i = 0;
            foreach (Article a in Articles)
            {
                i += a.CountWords(word);
            }
            return i;
        }

        public int CountArticles(string word)
        {
            int i = 0;
            foreach (Article a in Articles)
            {
                if (a.Contains(word)) i++;
            }
            return i;
        }
        public int CountArticles(List<string> words)
        {
            int i = 0;
            foreach (string word in words)
                foreach (Article a in Articles)
                {
                    if (a.Contains(word)) i++;
                }
            return i;
        }

        // Counts the articles that contain both word1 and word2
        public int WordMatch(List<string> words1, List<string> words2)
        {
            int i = 0;
            foreach (string word1 in words1)
                foreach (string word2 in words2)
                    i += WordMatch(word1, word2);
            return i;
        }

        // Counts the articles that contain both word1 and word2
        public int WordMatch(string word1, string word2)
        {
            int i = 0;
            foreach (Article a in Articles)
            {
                if (a.Contains(word1) && a.Contains(word2)) i++;
            }
            return i;
        }

        public void Print(StreamWriter streamWriter, List<string> primaryPandemic, List<string> secondaryPandemic, List<string> primarySource, List<string> secondarySource, List<string> tertiarySource)
        {
            streamWriter.Write(Date.Date.ToString("dd/MM/yyyy"));
            streamWriter.Write("," + FirstOrderCoronaArticles.Count);
            streamWriter.Write("," + SecondOrderCoronaArticles.Count);
            streamWriter.Write("," + CountOccurences(primarySource, FirstOrderCoronaArticles));
            streamWriter.Write("," + CountOccurences(primarySource, SecondOrderCoronaArticles));
            streamWriter.Write("," + CountOccurences(secondarySource, FirstOrderCoronaArticles));
            streamWriter.Write("," + CountOccurences(secondarySource, SecondOrderCoronaArticles));
            streamWriter.Write("," + CountOccurences(tertiarySource, FirstOrderCoronaArticles));
            streamWriter.Write("," + CountOccurences(tertiarySource, SecondOrderCoronaArticles));
            foreach (string s in primaryPandemic)
            {
                streamWriter.Write("," + CountWordOccurences(s, Articles));
            }
            foreach (string s in secondaryPandemic)
            {
                streamWriter.Write("," + CountWordOccurences(s, Articles));
            }
            foreach (string s in primarySource)
            {
                streamWriter.Write("," + CountWordOccurences(s, FirstOrderCoronaArticles));
                streamWriter.Write("," + CountWordOccurences(s, SecondOrderCoronaArticles));
            }
            foreach (string s in secondarySource)
            {
                streamWriter.Write("," + CountWordOccurences(s, FirstOrderCoronaArticles));
                streamWriter.Write("," + CountWordOccurences(s, SecondOrderCoronaArticles));
            }
            foreach (string s in tertiarySource)
            {
                streamWriter.Write("," + CountWordOccurences(s, FirstOrderCoronaArticles));
                streamWriter.Write("," + CountWordOccurences(s, SecondOrderCoronaArticles));
            }
            streamWriter.WriteLine();
        }

        private int CountOccurences(List<string> words, List<Article> articles)
        {
            int i = 0;
            foreach (Article a in articles)
                foreach (string s in words)
                {
                    if (a.Contains(s))
                    {
                        i++;
                        break;
                    }
                }
            return i;
        }

        private int CountWordOccurences(string word, List<Article> articles)
        {
            int i = 0;
            foreach (Article a in articles)
                if (a.Contains(word)) i++;
            return i;
        }
    }

    public class ArchiveNUpuntNL
    {
        public List<ArticleNUpuntNLPreProcessing> articles = new List<ArticleNUpuntNLPreProcessing>();


        public List<Day> ToDays(List<string> primaryPandemic, List<string> secondaryPandemic)
        {
            List<Day> days = new List<Day>();
            foreach (ArticleNUpuntNLPreProcessing a in articles)
            {
                int i = getIndex(a.toDateTime(), days);
                if (i == -1)
                {
                    days.Add(new Day(new List<Article>(), a.toDateTime(), new List<Article>(), new List<Article>()));
                    i = getIndex(a.toDateTime(), days);
                }
                days[i].AddIfNotNull(a.ToArticle(), primaryPandemic, secondaryPandemic);
            }
            days.Sort();
            return days;
        }

        public int getIndex(DateTime d, List<Day> days)
        {
            for (int i = days.Count - 1; i >= 0; i--)
            {
                if (days[i].Date == d) return i;
            }
            return -1;
        }
    }

    public class ArticleNUpuntNLPreProcessing
    {
        public string Title;
        public string Body;
        public string Date;

        public Article ToArticle()
        {
            return new Article(Title, Body);
        }

        public DateTime toDateTime()
        {
            //DateTime.TryParse(Date, out Date date)
            return new DateTime(2000 + Convert.ToInt32(Date[6..8]), Convert.ToInt32(Date[3..5]), Convert.ToInt32(Date[..2]));
        }
    }

}
